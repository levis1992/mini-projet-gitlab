FROM nginx:1.21.1
LABEL maintainer="Ballo youssouf"
RUN apt-get update -y  && \
    apt-get upgrade -y && \
    apt-get install -y git curl
RUN rm -Rf /usr/share/nginx/html/*
RUN git clone https://github.com/diranetafen/static-website-example.git /usr/share/nginx/html
CMD ["nginx", "-g", "daemon off;"]